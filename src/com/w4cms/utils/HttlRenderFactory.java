package com.w4cms.utils;

import httl.web.WebEngine;

import com.jfinal.core.JFinal;
import com.jfinal.render.IMainRenderFactory;
import com.jfinal.render.Render;

public class HttlRenderFactory implements IMainRenderFactory {

	public Render getRender(String view) {
		return new HttlRender(view);
	}

	public String getViewExtension() {
		return WebEngine.getTemplateSuffix(JFinal.me().getServletContext());
	}

}
