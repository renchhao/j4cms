package com.w4cms.utils;

import httl.web.WebEngine;

import com.jfinal.render.Render;
import com.jfinal.render.RenderException;

/**
 * HttlRender. (Integration, Prototype, ThreadSafe)
 * 
 * @author dafei (myaniu AT gmail DOT com)
 */
public class HttlRender extends Render {

	private static final long serialVersionUID = -7218493570717379375L;

	public HttlRender(String view) {
		this.view = view;
	}

	@Override
	public void render() {
		try {
			WebEngine.setRequestAndResponse(request, response);
			WebEngine.getEngine().getTemplate(this.view, request.getLocale()).render(response);
		} catch (Exception e) {
			throw new RenderException(e.getMessage(), e);
		}
	}

}