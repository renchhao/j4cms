package com.w4cms.utils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import com.jfinal.plugin.activerecord.Page;

public class Tools {
	/**
	 * 检测字符串是否不为空(null,"","null")
	 * @param s
	 * @return 不为空则返回true，否则返回false
	 */
	public static boolean notEmpty(String s){
		return s!=null && !"".equals(s) && !"null".equals(s);
	}
	
	/**
	 * 检测字符串是否为空(null,"","null")
	 * @param s
	 * @return 为空则返回true，不否则返回false
	 */
	public static boolean isEmpty(String s){
		return s==null || "".equals(s) || "null".equals(s);
	}
	
	/**
	 * 字符串转换为字符串数组
	 * @param str 字符串
	 * @param splitRegex 分隔符
	 * @return
	 */
	public static String[] str2StrArray(String str,String splitRegex){
		if(isEmpty(str)){
			return null;
		}
		return str.split(splitRegex);
	}
	
	/**
	 * 用默认的分隔符(,)将字符串转换为字符串数组
	 * @param str	字符串
	 * @return
	 */
	public static String[] str2StrArray(String str){
		return str2StrArray(str,",\\s*");
	}
	
	/**
	 * 按照yyyy-MM-dd HH:mm:ss的格式，日期转字符串
	 * @param date
	 * @return yyyy-MM-dd HH:mm:ss
	 */
	public static String date2Str(Date date){
		return date2Str(date,"yyyy-MM-dd HH:mm:ss");
	}
	
	/**
	 * 按照yyyy-MM-dd HH:mm:ss的格式，字符串转日期
	 * @param date
	 * @return
	 */
	public static Date str2Date(String date){
		if(notEmpty(date)){
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			try {
				return sdf.parse(date);
			} catch (ParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			return new Date();
		}else{
			return null;
		}
	}
	
	/**
	 * 按照参数format的格式，日期转字符串
	 * @param date
	 * @param format
	 * @return
	 */
	public static String date2Str(Date date,String format){
		if(date!=null){
			SimpleDateFormat sdf = new SimpleDateFormat(format);
			return sdf.format(date);
		}else{
			return "";
		}
	}
	public static String getPageStr(Page pages){
		StringBuffer sb = new StringBuffer();
		if(pages.getTotalRow()>0){
			sb.append("	<ul>\n");
			if(pages.getPageNumber()==1){
				sb.append("	<li class=\"pg_index\"><a>首页</a></li>\n");
				sb.append("	<li class=\"pg_index\"><a>上页</a></li>\n");
			}else{	
				sb.append("	<li><a href=\"javascript:;\" onclick=\"nextPage(1)\">首页</a></li>\n");
				sb.append("	<li><a href=\"javascript:;\" onclick=\"nextPage("+(pages.getPageNumber()-1)+")\">上页</a></li>\n");
			}
			int showTag = 3;	//分页标签显示数量
			int startTag = 1;
			if(pages.getTotalPage()>showTag){
				startTag = pages.getPageNumber()-1;
				startTag = startTag<1?1:startTag;
			}
			int endTag = startTag+showTag-1;
			for(int i=startTag; i<=pages.getTotalPage() && i<=endTag; i++){
				if(pages.getPageNumber()==i)
					sb.append("<li><a class=\"pg_selected\">"+i+"</a></li>\n");
				else
					sb.append("	<li><a href=\"javascript:;\" onclick=\"nextPage("+i+")\">"+i+"</a></li>\n");
			}
			if(pages.getPageNumber()==pages.getTotalPage()){
				sb.append("	<li class=\"pg_last\"><a>下页</a></li>\n");
				sb.append("	<li class=\"pg_last\"><a>尾页</a></li>\n");
			}else{
				sb.append("	<li><a href=\"javascript:;\" onclick=\"nextPage("+(pages.getPageNumber()+1)+")\">下页</a></li>\n");
				sb.append("	<li><a href=\"javascript:;\" onclick=\"nextPage("+pages.getTotalPage()+")\">尾页</a></li>\n");
			}
			sb.append("	<li><a>第"+pages.getPageNumber()+"页</a></li>\n");
			sb.append("	<li><a>共"+pages.getTotalPage()+"页</a></li>\n");
			sb.append("</ul>\n");
			sb.append("<p>共有"+pages.getTotalRow()+" 条数据，当前第"+pages.getPageNumber()+"页</p>\n");
			sb.append("<script type=\"text/javascript\">\n");
			sb.append("function nextPage(page){\n");
			sb.append("	if(true && document.forms[0]){\n");
			sb.append("		var url = document.forms[0].getAttribute(\"action\");\n");
			sb.append("		if(url.indexOf('?')>-1){url += \"&p=\";}\n");
			sb.append("		else{url += \"?p=\";}\n");
			sb.append("		document.forms[0].action = url+page;\n");
			sb.append("		document.forms[0].submit();\n");
			sb.append("	}else{\n");
			sb.append("		var url = document.location+'';\n");
			sb.append("		if(url.indexOf('?')>-1){\n");
			sb.append("			if(url.indexOf('currentPage')>-1){\n");
			sb.append("				var reg = /currentPage=\\d*/g;\n");
			sb.append("				url = url.replace(reg,'currentPage=');\n");
			sb.append("			}else{\n");
			sb.append("				url += \"&p=\";\n");
			sb.append("			}\n");
			sb.append("		}else{url += \"?p=\";}\n");
			sb.append("		document.location = url + page;\n");
			sb.append("	}\n");
			sb.append("}\n");
			sb.append("</script>\n");
		}
		return sb.toString(); 
	}
}
