package com.w4cms.core;

import java.util.List;

import com.jfinal.ext.plugin.config.ConfigKit;
import com.jfinal.kit.PathKit;
import com.qiniu.api.config.Config;
import com.w4cms.model.SysMenu;

public class Consts {
	// 缓存时间 20分钟
		public static final int CACHE_TIME_MINI = 1200000;
		// 缓存时间 1小时
		public static final int CACHE_TIME_MAX = 3600000;
		// Http缓存时间 一个月
		public static final long HTTP_CACHE_TIME = 1000 * 60 * 60 * 24L;
		// 用户session key
		public static final String SESSION_USER = "session_user";
		public static final String SESSION_USER_NAME = "session_username";
		public static final String SESSION_USER_RIGHTS = "u_rights";
		public static final String SESSION_ROLE_RIGHTS = "r_rights";
		public static List<SysMenu> SESSION_MENUS = null;
		public static final String NO_INTERCEPTOR_PATH = ".*/((login)|(logout)|(code)|(index.html)|(index)).*";	//不对匹配该值的访问路径拦截（正则）
		// 分页大小
		public static final int BLOG_PAGE_SIZE = 8;
		// domain
		public static String DOMAIN_COOKIE = ConfigKit.getStr("domain");
		public static String DOMAIN_NAME   = ConfigKit.getStr("domain.name");
		public static String DOMAIN_URL	= "http://".concat(DOMAIN_NAME);
		// gravatar url, 七牛的gravatar镜像
		public static String PHOTO_URL	= "http://".concat(ConfigKit.getStr("gravatar.url")).concat("/avatar/");
		// 安装状态
		public static boolean IS_INSTALL = false;
		
		// tmpfsPath
		public static final String TMP_FS_PATH = PathKit.getWebRootPath() + "/temp";
		
		// bucket
		public static final String QINIU_BUCKET = ConfigKit.getStr("qiniu.bucket");
		
		static {
			Config.ACCESS_KEY = ConfigKit.getStr("qiniu.access.key");
			Config.SECRET_KEY = ConfigKit.getStr("qiniu.secret.key");
		}
		
		// ajax 状态
		public static final String AJAX_STATUS = "status";
		public static final int AJAX_Y = 0; //OK
		public static final int AJAX_N = 1; //no
		public static final int AJAX_S = 2; //权限
		public static final int AJAX_O = 3; //other 其他错误
}
