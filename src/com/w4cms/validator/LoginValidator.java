package com.w4cms.validator;

import com.jfinal.core.Controller;
import com.jfinal.validate.Validator;

public class LoginValidator extends Validator{

	@Override
	protected void validate(Controller c) {
		setShortCircuit(true);
		validateString("username", true, 2, 10, "msg", "用户名不能为空或者长度不符合");
		validateString("password", true, 6, 16, "status", "密码不能为空或者长度不符合");
	}
	@Override
	protected void handleError(Controller c) {
		c.keepPara();
		c.setAttr("msg", "用户名或者密码不能为空.");
		c.render("login.html");
	}

}
