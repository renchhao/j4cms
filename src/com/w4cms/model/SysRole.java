package com.w4cms.model;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.jfinal.plugin.activerecord.Model;
import com.jfinal.plugin.activerecord.Page;
import com.w4cms.utils.Tools;

public class SysRole extends Model<SysRole> {
	private static final long serialVersionUID = -3425960124315493588L;
	public static final SysRole dao = new SysRole();
	/**
	 * 获取角色列表
	 * @param id
	 * @return
	 */
	public List<SysRole> getRoleListById(Object id){
		if(id==null){
			return dao.find("select * from sys_role");
		}else{
			return dao.find("select * from sys_role where id=?",id);
		}
	}
	public boolean saveRole(){
		return this.set("create_time", new Date()).set("update_time", new Date()).save();
	}
	public boolean updateRole(){
		return this.set("update_time", new Date()).update();
	}
	/**
	 * 查询角色
	 * @param pageNumber
	 * @param pageSize
	 * @param search 角色名称
	 * @return
	 */
	public Page<SysRole> searchRoles(int pageNumber, int pageSize,String search){
		final List<Object> paras = new ArrayList<Object>(); 
		String select = "select * ";
		StringBuffer sqlExceptSelect = new StringBuffer(" from sys_role where 1=1 ");
		if(!Tools.isEmpty(search)){
			sqlExceptSelect.append("and (name like ?) ");
			paras.add("%"+search+"%");
		}
		return dao.paginate(pageNumber, pageSize, select, sqlExceptSelect.toString(), paras.toArray());
	}
	
}
