package com.w4cms.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
/**
 *  TreeJson 类
 *  @author wch (185656156@qq.com)
 *  @version V0.0.1  2013-10-14
 *  <table style="border:1px solid gray;;text-align:center">
 *          <tr>
 *          <th width="100px">版本号</th>
 *          <th width="100px">动作</th>
 *          <th width="100px">修改人</th>
 *          <th width="100px">修改时间</th>
 *          </tr>
 *          <!-- 以 Table 方式书写修改历史 -->
 *          <tr>
 *          <td>0.0.0</td>
 *          <td>创建类</td>
 *          <td>wch</td>
 *          <td>2013-10-14 上午10:37:25</td>
 *          </tr>
 *    </table>
 */
public class TreeData implements Serializable {
	private static final long serialVersionUID = 3678895719479399493L;
	private String id;
	private String pid;
	private String text;
	private String iconCls;
	private String state;
	private String checked;
	private String url;
	private Map<String,Object> attributes = new HashMap<String, Object>();
	private List<TreeData> children = new ArrayList<TreeData>();
	/**
	 * 最新treejson 构造树
	 * @param list
	 * @return
	 */
	public static List<TreeData> formatTree3(List<TreeData> list) {
		TreeData root = new TreeData();
		List<TreeData> treelist = new ArrayList<TreeData>();// 拼凑好的json格式的数据
		Map<String,Object> used = new HashMap<String, Object>();
		for(int i=0;i<list.size();i++){
			root = list.get(i);
			root = getParents3(list,used,root);
			if(root!=null){
				treelist.add(root);
			}
		}
		return treelist;
	}
	public static TreeData getParents3(List<TreeData> list,Map<String,Object> used,TreeData root){
		TreeData node = null;
		for(int i=0;i<list.size();i++){
			node = list.get(i);
			if(root.getPid().equals(node.getId())){
				node.children.add(root);
				return  null;
			}
		}
		return root;
	}

	/**
	 * @return 获取 id
	 */
	public String getId() {
		return id;
	}

	/**
	 * @param 设置 id id 
	 */
	public void setId(String id) {
		this.id = id;
	}

	/**
	 * @return 获取 pid
	 */
	public String getPid() {
		return pid;
	}

	/**
	 * @param 设置 pid pid 
	 */
	public void setPid(String pid) {
		this.pid = pid;
	}

	/**
	 * @return 获取 text
	 */
	public String getText() {
		return text;
	}

	/**
	 * @param 设置 text text 
	 */
	public void setText(String text) {
		this.text = text;
	}

	/**
	 * @return 获取 iconCls
	 */
	public String getIconCls() {
		return iconCls;
	}

	/**
	 * @param 设置 iconCls iconCls 
	 */
	public void setIconCls(String iconCls) {
		this.iconCls = iconCls;
	}

	/**
	 * @return 获取 state
	 */
	public String getState() {
		return state;
	}

	/**
	 * @param 设置 state state 
	 */
	public void setState(String state) {
		this.state = state;
	}

	/**
	 * @return 获取 checked
	 */
	public String getChecked() {
		return checked;
	}

	/**
	 * @param 设置 checked checked 
	 */
	public void setChecked(String checked) {
		this.checked = checked;
	}

	/**
	 * @return 获取 attributes
	 */
	public Map<String, Object> getAttributes() {
		return attributes;
	}
	/**
	 * @param 设置 attributes attributes 
	 */
	public void setAttributes(Map<String, Object> attributes) {
		this.attributes = attributes;
	}
	/**
	 * @return 获取 children
	 */
	public List<TreeData> getChildren() {
		return children;
	}

	/**
	 * @param 设置 children children 
	 */
	public void setChildren(List<TreeData> children) {
		this.children = children;
	}
	/**
	 * @return 获取 url
	 */
	public String getUrl() {
		return url;
	}
	/**
	 * @param 设置 url url 
	 */
	public void setUrl(String url) {
		this.url = url;
	}
	
	
	
	
}
