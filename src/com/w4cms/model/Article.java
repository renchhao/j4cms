package com.w4cms.model;

import java.util.ArrayList;
import java.util.List;

import com.jfinal.plugin.activerecord.Model;
import com.jfinal.plugin.activerecord.Page;
import com.w4cms.utils.Tools;

public class Article  extends Model<Article>{
	private static final long serialVersionUID = 3505467521894526014L;
	public static final Article dao = new Article();
	public Page<Article> searchArticles(int pageNumber, int pageSize,String search,String cateId){
		final List<Object> paras = new ArrayList<Object>(); 
		String select = "select u.id,u.username,a.id,a.title,a.sub_title,a.comefrom,a.content,a.view_count,a.create_time,a.update_time";
		StringBuffer sqlExceptSelect = new StringBuffer(" from article a left join sys_user u on a.user_id=u.id where 1=1 ");
		if(!Tools.isEmpty(search)){
			sqlExceptSelect.append("and (a.title like ? or a.content like ?) ");
			paras.add("%"+search+"%");
			paras.add("%"+search+"%");
		}
		if(!Tools.isEmpty(cateId) && !"0".equals(cateId)){
			sqlExceptSelect.append("and (a.category_id=? or a.topic_id=?)");
			paras.add(cateId);
			paras.add(cateId);
		}
		return dao.paginate(pageNumber, pageSize, select, sqlExceptSelect.toString(), paras.toArray());
	}
}
