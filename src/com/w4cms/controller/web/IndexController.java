package com.w4cms.controller.web;

import java.util.List;

import com.jfinal.core.Controller;
import com.jfinal.ext.route.ControllerBind;
import com.w4cms.model.SysUser;
@ControllerBind(controllerKey="/")
public class IndexController extends Controller {
	
	public void index(){
		List<SysUser> us = SysUser.dao.find("select * from sys_user");
		setAttr("users", us);
		render("index.html");
	}
	public void work(){
		List<SysUser> us = SysUser.dao.find("select * from sys_user");
		setAttr("users", us);
		render("index.html");
	}
	public void blog(){
		List<SysUser> us = SysUser.dao.find("select * from sys_user");
		setAttr("users", us);
		render("index.html");
	}
	public void process(){
		List<SysUser> us = SysUser.dao.find("select * from sys_user");
		setAttr("users", us);
		render("index.html");
	}
	public void contact(){
		List<SysUser> us = SysUser.dao.find("select * from sys_user");
		setAttr("users", us);
		render("index.html");
	}
}
