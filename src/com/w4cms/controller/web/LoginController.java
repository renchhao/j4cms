package com.w4cms.controller.web;

import com.jfinal.core.ActionKey;
import com.jfinal.core.Controller;


public class LoginController extends Controller {
	@ActionKey("/me/login")
	public void login(){
		renderText("登录.");
	}
}
