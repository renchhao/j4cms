package com.w4cms.controller.admin;

import java.util.Date;
import java.util.List;

import com.jfinal.aop.Before;
import com.jfinal.core.Controller;
import com.jfinal.ext.route.ControllerBind;
import com.jfinal.plugin.activerecord.Page;
import com.w4cms.interceptor.AdminInterceptor;
import com.w4cms.model.SysMenu;
import com.w4cms.model.TreeData;
import com.w4cms.utils.Tools;

@ControllerBind(controllerKey="/admin/menu",viewPath="admin/menu")
@Before(AdminInterceptor.class)
public class MenuController extends Controller {
	public void list(){
		this.search();
	}
	public void add(){
		SysMenu m = SysMenu.dao.findFirst("select max(menu_order) as maxOrder from sys_menu");
		List<TreeData> menus = SysMenu.dao.menuTree(null);
		setAttr("menus", menus);
		setAttr("order", m.getInt("maxOrder")!=null?m.getInt("maxOrder")+1:0);
		render("menuinfo.html");
	}
	public void edit(){
		Object id = getPara();
		SysMenu menu = SysMenu.dao.findById(id);
		List<TreeData> menus = SysMenu.dao.menuTree(null);
		setAttr("menus", menus);
		setAttr("obj", menu);
		render("menuinfo.html");
	}
	public void save(){
		String id = getPara("id");
		boolean isOk = false;
		if(Tools.isEmpty(id)){
			isOk = new SysMenu().set("name", getPara("name")).set("url", getPara("url")).set("menu_order", getPara("order")).set("remark", getPara("remark"))
					.set("status", getPara("status")).set("parent_id", getPara("pid")).set("create_time", new Date()).set("update_time", new Date()).save();
		}else{
			isOk = new SysMenu().set("id", id).set("name", getPara("name")).set("url", getPara("url")).set("menu_order", getPara("order")).set("remark", getPara("remark"))
					.set("status", getPara("status")).set("parent_id", getPara("pid")).set("update_time", new Date()).update();
		}
		if(isOk){
			renderJson("success",true);
		}else{
			renderJson("msg","操作失败.");
		}
	}
	
	public void delete(){
		Object id= getPara();
		boolean isOk = new SysMenu().set("id", id).set("status", -1).set("update_time", new Date()).update();
		if(isOk){
			renderText("true");
		}else{
			renderText("删除菜单失败.");
		}
	}
	
	public void disable(){
		Object id= getPara();
		boolean isOk = new SysMenu().set("id", id).set("status", 1).set("update_time", new Date()).update();
		if(isOk){
			renderText("true");
		}else{
			renderText("停用菜单失败.");
		}
	}
	public void enable(){
		Object id= getPara();
		boolean isOk = new SysMenu().set("id", id).set("status", 0).set("update_time", new Date()).update();
		if(isOk){
			renderText("true");
		}else{
			renderText("启用菜单失败.");
		}
	}
	
	public void search(){
		String search = getPara("search");
		List<TreeData> list = SysMenu.dao.searchMenuTree(search);
		setAttr("menus", list);
		keepPara();
		render("menulist.html");
	}
	public void search2(){
		String search = getPara("search");
		Integer pageNumber = getParaToInt("p",1);
		if(pageNumber<1){
			pageNumber=1;
		}
		Integer pageSize = getParaToInt("r",15);
		if(pageSize<1 || pageSize>100){
			pageSize = 15;
		}
		Page<SysMenu> list = SysMenu.dao.searchMenus(pageNumber, pageSize, search);
		setAttr("menus", list);
		setAttr("pageStr", Tools.getPageStr(list));
		keepPara();
		render("menulist.html");
	}
	
}
