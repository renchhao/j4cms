/*==============================================================*/
/* DBMS name:      MySQL 5.0                                    */
/* Created on:     2014-04-17 15:00:33                          */
/*==============================================================*/


drop table if exists article;

drop table if exists banner;

drop table if exists category;

drop table if exists sys_menu;

drop table if exists sys_role;

drop table if exists sys_user;

drop table if exists topic;

/*==============================================================*/
/* Table: article                                               */
/*==============================================================*/
create table article
(
   id                   varchar(36) not null comment '主键',
   title                varchar(200) comment '文章标题',
   sub_title            varchar(100) comment '文章副标题',
   user_id              int comment '文章创建者主键',
   comefrom             varchar(100) comment '文章来源',
   content              text comment '文章内容',
   view_count           int default 0 comment '文章浏览次数',
   status               int default 0 comment '文章状态',
   category_id          varchar(36) comment '文章分类主键',
   topic_id             varchar(36) comment '文章主题主键',
   create_time          datetime comment '创建时间',
   update_time          datetime comment '更新时间',
   primary key (id)
);

alter table article comment '文章信息表';

/*==============================================================*/
/* Table: banner                                                */
/*==============================================================*/
create table banner
(
   id                   varchar(36) not null comment '主键',
   title                varchar(20) comment '标题',
   sub_title            varchar(20) comment '副标题',
   link_url             varchar(200) comment '连接地址',
   pic_url              varchar(200) comment '图片地址',
   status               int default 0 comment '状态',
   user_id              int comment '创建者主键',
   create_time          datetime comment '创建时间',
   update_time          datetime comment '更新时间',
   primary key (id)
);

alter table banner comment '轮播图片信息表';

/*==============================================================*/
/* Table: category                                              */
/*==============================================================*/
create table category
(
   id                   varchar(36) not null comment '主键',
   name                 varchar(100) comment '分类名称',
   remark               varchar(200) comment '分类备注',
   cate_order           int,
   status               int default 0 comment '分类状态',
   user_id              int comment '分类创建者主键',
   create_time          datetime comment '创建时间',
   update_time          datetime comment '更新时间',
   primary key (id)
);

alter table category comment '文章分类表';

/*==============================================================*/
/* Table: sys_menu                                              */
/*==============================================================*/
create table sys_menu
(
   id                   int not null auto_increment comment '主键',
   name                 varchar(100) comment '菜单名称',
   url                  varchar(100) comment '菜单连接',
   menu_order           int default 0 comment '菜单排序',
   remark               varchar(100) comment '菜单备注',
   parent_id            int comment '菜单父节点主键',
   create_time          datetime comment '创建时间',
   update_time          datetime comment '更新时间',
   primary key (id)
);

alter table sys_menu comment '系统菜单信息表';

/*==============================================================*/
/* Table: sys_role                                              */
/*==============================================================*/
create table sys_role
(
   id                   int not null auto_increment comment '主键',
   name                 varchar(20) comment '角色名称',
   rights               varchar(100) comment '角色权限',
   remark               varchar(100) comment '角色备注',
   create_time          datetime comment '创建时间',
   update_time          datetime comment '更新时间',
   primary key (id)
);

alter table sys_role comment '角色信息表';

/*==============================================================*/
/* Table: sys_user                                              */
/*==============================================================*/
create table sys_user
(
   id                   int not null auto_increment comment '用户主键',
   username             varchar(20) comment '用户登录名',
   password             varchar(64) comment '用户密码',
   email                varchar(50) comment '用户邮箱',
   email_verify         int default 0 comment '用户邮箱验证',
   head_photo           varchar(100) comment '头像',
   sex                  int default 0 comment '性别',
   birthday             date comment '生日',
   status               int default 0 comment '状态',
   signature            varchar(100) comment '签名',
   role_id              int comment '角色主键',
   rights               varchar(100) comment '菜单权限',
   last_login           datetime comment '最后登录时间',
   create_time          datetime comment '创建时间',
   update_time          datetime comment '修改时间',
   primary key (id)
);

alter table sys_user comment '系统用户信息表';

/*==============================================================*/
/* Table: topic                                                 */
/*==============================================================*/
create table topic
(
   id                   varchar(36) not null comment '主键',
   title                varchar(100) comment '主题标题',
   content              text comment '主题内容',
   status               int default 0 comment '主题状态',
   user_id              int comment '主题创建者主键',
   create_time          datetime comment '创建时间',
   update_time          datetime comment '更新时间',
   primary key (id)
);

alter table topic comment '主题信息表';

